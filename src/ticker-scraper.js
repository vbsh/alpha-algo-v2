import axios from 'axios';
import { url } from './config';
import { initParser } from './parser';
import { stateManager } from './state-manager';

const getHtml = async (url) => {
    const res = await axios.get(url);
    return res.data;
}

export const getLastTicker = async () => {
   while (true) {
            try {
                let html = await getHtml(url);
                let parser = initParser();
                await parser.write(html);
                parser.end();
                await delay(5000);
            } catch (e) {
                console.log("error");
            }
    }
}

const delay = ms => new Promise(resolve => setTimeout(resolve, ms))
