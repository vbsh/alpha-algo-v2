import {writeFileSync, readFileSync, writeFile} from 'fs';
import {coinsHistoryFileName} from '../config'

export function writeCoinDataToJsonFile(tradeAmount,coinName) {
    try {
        writeFileSync('coin-data.txt', `{"tradeAmount":${tradeAmount},"coinName":"${coinName}"}`);
    } catch (e) {
        console.log(e);
    }
}

export function addCoinToHistory(coinPair) {
    try {
        console.log("save " + coinPair + "to history")
        writeFileSync(coinsHistoryFileName, coinPair);
        console.log("saved succesfully " + coinPair + "to history")
    } catch (e) {
        console.log("Feild write coin pair to history");
    }
}

export function getLastCoinFromHistory() {
    try {
        return readFileSync(coinsHistoryFileName);
    } catch (e) {
        console.log("Feild read from coin history");
    }
}