import {wordsToExclude, wordsToInclude, buyToCoin} from "./config";
import {isSubstringInString} from "./utils";
import htmlparser from 'htmlparser2-es6';
import {trySellCoin as trySellCoin} from "./binance";
import axios from 'axios'
import {getLastCoinFromHistory} from './file-utils';
import {stateManager} from "./state-manager";

let isTryToSell = false;
let numOfTickers = 0;

export function initParser() {
    numOfTickers = 0;
    return new htmlparser({
        ontext: async (htmlText) => {

            // if (shouldProcessLine(htmlText) && numOfTickers === 0 && !isTryToSell) {

            if (numOfTickers === 0 && !isTryToSell) {
                stateManager.shouldRunHtmlAgain = false;
                // const {ticker, tokenName} = extractTickerFromLine(htmlText);
                const ticker = "ENS"; //We have problem in getting the contract adress - TO DO - NEW mETHOD!!!
                const tokenName = "Ethereum Name Service";
                var now = new Date();
                console.log(`ticker: |${ticker}| , tokenName |${tokenName}| , |${now}`);
                numOfTickers++;
                let lastCoinPair = getLastCoinFromHistory();
                if (lastCoinPair.includes(ticker)) {
                    return;
                }
                const {tokenAddress, platformSymbol} = await getTokenAddressByTokenSymbol(tokenName);
                console.log(`[${now}] tokenAddress: ${tokenAddress} \n `);
                isTryToSell = true;
                console.log()
                await trySellCoin(ticker, tokenAddress);
                stateManager.shouldRunHtmlAgain = true
            }
        },
    });
}

async function getTokenAddressByTokenSymbol(tokenSymbol) {
    const response = await axios.get("https://pro-api.coinmarketcap.com/v1/cryptocurrency/map", {headers: {'X-CMC_PRO_API_KEY': '902dc51c-f3b9-41e7-ae83-5c6410d6414d'}});
    const data = response.data.data;

    for (let index = 0; index < data.length; index++) {
        const element = response.data.data[index];
        if (element.name === tokenSymbol) {
            return {tokenAddress: element.platform.token_address, platformSymbol: element.platform.symbol};
        }
    }
}

/*

// Download the helper library from https://www.twilio.com/docs/node/install
// Find your Account SID and Auth Token at twilio.com/console
// and set the environment variables. See http://twil.io/secure
const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const client = require('twilio')(accountSid, authToken);

client.calls
      .create({
         url: 'http://demo.twilio.com/docs/voice.xml',
         to: '+123456789',
         from: '+987654321'
       })
      .then(call => console.log(call.sid));


*/

const extractTickerFromLine = (text) => {
    let startIndex = text.indexOf("(")
    const endIndex = text.indexOf(")")
    console.log(text)
    const ticker = text.substring(startIndex + 1, endIndex);
    text = text.replace('Binance Will List ', '');
    startIndex = text.indexOf("(")

    let tokenName = text.substring(0, startIndex - 1);
    return {ticker, tokenName};
}

const shouldProcessLine = (text) => {
    let shouldProcess = true;

    wordsToInclude.forEach((word) => {
        if (!isSubstringInString(word, text)) {
            shouldProcess = false;
        }
    });

    wordsToExclude.forEach((word) => {
        if (isSubstringInString(word, text)) {
            shouldProcess = false;
        }
    })

    return shouldProcess;
}
