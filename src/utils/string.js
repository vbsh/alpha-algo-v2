import { increasingStopLossPercent } from '../config';
import { getPriceByCoinPer } from '../binance'

export const isSubstringInString = (substring, string) => {
    if (string.indexOf(substring) != -1) {
        return true;
    }

    return false;
}

export async function shouldPositionBeClosed(coinPair, currentStopLoss, currentPeek) {
    let newStopLoss = currentStopLoss;
    let newPeek = currentPeek;
    let shouldSell = false;
    let currentPrice = await getPriceByCoinPer(coinPair);

    if (currentPrice < currentStopLoss) {
        shouldSell = true;
    }

    if (currentPrice > currentPeek) {
        newPeek = currentPeek * (1 + increasingStopLossPercent);
        newStopLoss = currentStopLoss + (newPeek - currentPeek);
    }

    return { shouldSell, newStopLoss, newPeek };
}