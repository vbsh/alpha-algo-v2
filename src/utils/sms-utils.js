const xmlBuilder = require('xmlbuilder', {encoding: 'utf-8'});
const axios = require('axios');
const userName = 'ybublil';
const apiToken = '473439fc-e2cf-4272-bbe2-7ddc083b9a1b';
const sender = 'CryptoBot';

//
//  Usage example: the message and phoneNumbers should be strings.
//  The message can contain any string, the phone numbers can contain multiple numbers seperated by ; e.g. 0501111111;0541111111 or a single number 0501111111
//  sendSms('This is a message', '0501111111;0541111111');
//
export const sendSms = (message, phoneNumbers) => {
    const smsXml = createSms(message, phoneNumbers);
    const config = {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }
    axios.post('https://uapi.inforu.co.il/SendMessageXml.ashx', `InforuXML=${smsXml}`, config).then(res => {
        console.log(res.data)
    }).catch(err => console.log(err));
    console.log(smsXml);
}

const createSms = (message, phoneNumbers) => {
    const xmlObject = {
        Inforu: {
            User: {
                Username: {
                    '#text': userName,
                },
                ApiToken: {
                    '#text': apiToken,
                }
            },
            Content: {
                '@Type': 'sms',
                Message: {
                    '#text': message,
                }
            },
            Recipients: {
                PhoneNumber: {
                    '#text': phoneNumbers,
                }
            },
            Settings: {
                Sender: {
                    '#text': sender,
                }
            }
        }
    }
    return xmlBuilder.create(xmlObject, {version: '1.0', encoding: 'UTF-8'}).end({pretty: true});
}
