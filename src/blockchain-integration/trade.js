const {Token, ChainId, Fetcher, WETH, Route, Trade, TokenAmount, TradeType, Percent} = require("@pancakeswap/sdk");
const {JsonRpcProvider} = require("@ethersproject/providers");
const provider = new JsonRpcProvider('https://bsc-dataseed1.binance.org/'); //That
const ethers = require('ethers')
const PANCAKE_ROUTER = '0x10ed43c718714eb63d5aa57b78b54704e256024e'; //That
const BNB_TOKEN_DECIMALS = 18;
const Eth = require("web3-eth")
const eth = new Eth("https://bsc-dataseed1.binance.org:443"); //That

async function generateBNBTrade(amountIn, tokenContractAddress, tokenDecimals, slippagePercents, tradingAddress, privateKey, outgoing) {
    const TRADING_TOKEN = new Token(
        ChainId.MAINNET,
        tokenContractAddress,
        tokenDecimals ? tokenDecimals : BNB_TOKEN_DECIMALS
    );

    const pair = await Fetcher.fetchPairData(TRADING_TOKEN, WETH[TRADING_TOKEN.chainId], provider);
    const route = new Route([pair], outgoing ? WETH[TRADING_TOKEN.chainId] : TRADING_TOKEN);
    const inAmountInWei = (amountIn * (10 ** (outgoing ? BNB_TOKEN_DECIMALS : tokenDecimals))).toString();

    const trade = new Trade(
        route,
        new TokenAmount(outgoing ? WETH[TRADING_TOKEN.chainId] : TRADING_TOKEN, inAmountInWei),
        TradeType.EXACT_INPUT
    );

    const slippageTolerance = new Percent(slippagePercents.toString(), '10000')

    // create transaction parameters
    const amountOutMin = trade.minimumAmountOut(slippageTolerance).raw
    console.log("amount out min", (amountOutMin / (10 ** (outgoing ? tokenDecimals : BNB_TOKEN_DECIMALS))).toString());
    const pathPair = [WETH[TRADING_TOKEN.chainId].address, TRADING_TOKEN.address];
    const path = outgoing ? pathPair : pathPair.reverse();
    const to = tradingAddress;
    const deadline = Math.floor(Date.now() / 1000) + 60 * 20

    // Create signer
    const wallet = new ethers.Wallet(
        Buffer.from(
            privateKey,
            "hex"
        )
    )
    const signer = wallet.connect(provider)

    return {TRADING_TOKEN, inAmountInWei, amountOutMin, path, to, deadline, signer};
}

async function approvePancakeTrading(TRADING_TOKEN, signer) {
    // Allow Pancakeswap
    let abi = ["function approve(address _spender, uint256 _value) public returns (bool success)"]
    let contract = new ethers.Contract(TRADING_TOKEN.address, abi, signer)
    await contract.approve(PANCAKE_ROUTER, ethers.utils.parseUnits('1000.0', BNB_TOKEN_DECIMALS), {
        gasLimit: 100000,
        gasPrice: 5e9
    });
    console.log("contract approved");
}

/**
 * trade BNB for some token in exchange
 * @param bnbAmountIn the amount of BNB you want to trade, in full units (not wei) for example: 0.03 (BNB) - should be a number
 * @param tradingAddress the trading address (should be the address of your account which you provide private key for)
 * @param privateKey the private key for the trading account
 * @param tokenContractAddress the contract address of the token
 * @param tokenDecimals the decimals of the token
 * @param slippagePercents percents for slippage (read more in docs/google)
 * @param shouldApprove should try to approve the contract (Set to true if it's the first time you are using this contract/method)
 * @returns {Promise<void>}
 */
export const tradeBNBToTokenInPancakeSwap = async (bnbAmountIn, tradingAddress, privateKey, tokenContractAddress, tokenDecimals, slippagePercents, shouldApprove) => {
    const {
        TRADING_TOKEN,
        inAmountInWei,
        amountOutMin,
        path,
        to,
        deadline,
        signer,
    } = await generateBNBTrade(bnbAmountIn, tokenContractAddress, tokenDecimals, slippagePercents, tradingAddress, privateKey, true);

    const pancakeswap = new ethers.Contract(
        PANCAKE_ROUTER,
        ['function swapExactETHForTokens(uint256 amountOutMin, address[] path, address to, uint256 deadline) external payable returns (uint[] memory amounts)'],
        signer
    )

    if (shouldApprove) {
        await approvePancakeTrading(TRADING_TOKEN, signer);
    }

    // Execute transaction
    const tx = await pancakeswap.swapExactETHForTokens(
        ethers.utils.parseUnits((amountOutMin / (10 ** tokenDecimals)).toString(), tokenDecimals),
        path,
        to,
        deadline,
        {
            gasLimit: ethers.utils.hexlify(200000),
            gasPrice: ethers.utils.parseUnits("10", "gwei"),
            value: inAmountInWei
        }
    )

    console.log(`Tx-hash: ${tx.hash}`)
    console.log(`https://bscscan.com/tx/${tx.hash}`)
    await eth.getTransactionReceipt(tx.hash);
    return tx.hash;
};

/**
 * trade a token for BNB in exchange
 * @param tokenAmountIn the token amount that you want to trade, in full units (not wei), for example 20 (USDT) - should be a number
 * @param tradingAddress the address for the trading account (which you provide private key for)
 * @param privateKey the private key for the trading account
 * @param tokenContractAddress the contract address of the token
 * @param tokenDecimals the decimals of the token
 * @param slippagePercents percents for slippage
 * @param shouldApprove true if it's the first time you use this method for this account
 * @returns {Promise<void>}
 */
export const tradeTokenToBNBInPancakeSwap = async (tokenAmountIn, tradingAddress, privateKey, tokenContractAddress, tokenDecimals, slippagePercents, shouldApprove) => {
    const {
        TRADING_TOKEN,
        amountOutMin,
        path,
        to,
        deadline,
        signer,
    } = await generateBNBTrade(tokenAmountIn, tokenContractAddress, tokenDecimals, slippagePercents, tradingAddress, privateKey, false);

    const pancakeswap = new ethers.Contract(
        PANCAKE_ROUTER,
        ['function swapExactTokensForETH(uint amountIn, uint amountOutMin, address[] calldata path, address to, uint deadline) external returns (uint[] memory amounts)'],
        signer
    )

    if (shouldApprove) {
        await approvePancakeTrading(TRADING_TOKEN, signer);
    }

    // Execute transaction
    const tx = await pancakeswap.swapExactTokensForETH(
        ethers.utils.parseUnits(tokenAmountIn.toString(), tokenDecimals),
        ethers.utils.parseUnits((amountOutMin / (10 ** BNB_TOKEN_DECIMALS)).toString(), BNB_TOKEN_DECIMALS),
        path,
        to,
        deadline,
        {gasLimit: ethers.utils.hexlify(200000), gasPrice: ethers.utils.parseUnits("10", "gwei")}
    )

    console.log(`Tx-hash: ${tx.hash}`)
    console.log(`https://bscscan.com/tx/${tx.hash}`)
    await eth.getTransactionReceipt(tx.hash);
    return tx.hash;
};

/**
 * trade a token for another token in exchange
 * @param tokenAmountIn the token amount that you want to trade, in full units (not wei), for example 20 (USDT) - should be a number
 * @param tradingAddress the address for the trading account (which you provide private key for)
 * @param privateKey the private key for the trading account
 * @param tokenInContractAddress the contract address of the token you give away
 * @param tokenInDecimals the decimals of the token you give away
 * @param tokenOutContractAddress the contract address of the token you get
 * @param tokenOutDecimals the decimals of the token you give get
 * @param slippagePercents percents for slippage
 * @param shouldApprove true if it's the first time you use this method for this account
 * @returns {Promise<void>}
 */
export const tradeTokenForTokenInPancakeSwap = async (tokenAmountIn, tradingAddress, privateKey, tokenInContractAddress, tokenInDecimals,
                                                      tokenOutContractAddress, tokenOutDecimals, slippagePercents, shouldApprove) => {


    console.log(slippagePercents)
    const TOKEN_IN = new Token(
        ChainId.MAINNET,
        tokenInContractAddress,
        tokenInDecimals ? tokenInDecimals : BNB_TOKEN_DECIMALS
    );

    const TOKEN_OUT = new Token(
        ChainId.MAINNET,
        tokenOutContractAddress,
        tokenOutDecimals ? tokenOutDecimals : BNB_TOKEN_DECIMALS
    );

    const pair = await Fetcher.fetchPairData(TOKEN_IN, TOKEN_OUT, provider);
    const route = new Route([pair], TOKEN_IN);
    const inAmountInWei = (tokenAmountIn * (10 ** tokenInDecimals)).toString();
    console.log(inAmountInWei)
    const trade = new Trade(
        route,
        new TokenAmount(TOKEN_IN, inAmountInWei),
        TradeType.EXACT_INPUT
    );

    const slippageTolerance = new Percent(slippagePercents.toString(), '10000')

    // create transaction parameters
    const amountOutMin = trade.minimumAmountOut(slippageTolerance).raw
    console.log("amount out min", (amountOutMin / (10 ** tokenOutDecimals)).toString());
    const path = [TOKEN_IN.address, TOKEN_OUT.address];
    const to = tradingAddress;
    const deadline = Math.floor(Date.now() / 1000) + 60 * 20

    // Create signer
    const wallet = new ethers.Wallet(
        Buffer.from(
            privateKey,
            "hex"
        )
    )
    const signer = wallet.connect(provider)

    const pancakeswap = new ethers.Contract(
        PANCAKE_ROUTER,
        ['function swapExactTokensForTokens(uint amountIn, uint amountOutMin, address[] calldata path, address to, uint deadline) external returns (uint[] memory amounts)'],
        signer
    )

    if (shouldApprove) {
        await approvePancakeTrading(TOKEN_IN, signer);
    }

    // Execute transaction
    const tx = await pancakeswap.swapExactTokensForTokens(
        ethers.utils.parseUnits(tokenAmountIn.toString(), tokenInDecimals),
        ethers.utils.parseUnits((amountOutMin / (10 ** tokenOutDecimals)).toString(), tokenOutDecimals),
        path,
        to,
        deadline,
        {gasLimit: ethers.utils.hexlify(200000), gasPrice: ethers.utils.parseUnits("10", "gwei")}
    )

    console.log(`Tx-hash: ${tx.hash}`)
    console.log(`https://bscscan.com/tx/${tx.hash}`)
    console.log("Waiting for tx receipt");
    await eth.getTransactionReceipt(tx.hash);
    return tx.hash;

};

export const tradeBUSDForTokenInPancakeSwap = async (tokenAmountIn, tradingAddress, privateKey,
                                                     tokenOutContractAddress, tokenOutDecimals, slippage) => {
        const busd_address = "0xe9e7cea3dedca5984780bafc599bd69add087d56"
        return await tradeTokenForTokenInPancakeSwap(tokenAmountIn, tradingAddress, privateKey, busd_address, 18, tokenOutContractAddress, tokenOutDecimals, slippage, true)

}