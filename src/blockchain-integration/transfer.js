import Accounts from "web3-eth-accounts";
import Eth from "web3-eth";
import fs from "fs";

const ethers = require('ethers')
import path from "path";


/**
 * transfer BNB from the account with the provided private key, to an address
 * @param amount amount of BNB to transfer, in full units (not wei) for example: 0.03 (BNB) - should be a number
 * @param toAddress the destination BNB address to send to
 * @param pk private key of the sender account
 * @returns {Promise<void>}
 */
export const transferBNB = async (amount, toAddress, pk) => {
    const accountsBEP = new Accounts('https://bsc-dataseed1.binance.org:443');
    const ethBEP = new Eth("https://bsc-dataseed1.binance.org:443");

    const decimals = 18;
    const account = await accountsBEP.privateKeyToAccount(pk, true);
    console.log(account);
    const signedTx = await account.signTransaction({
        chainId: 56,
        from: account.address.toLowerCase(),
        to: toAddress,
        value: (amount * (10 ** decimals)).toString(),
        gasPrice: 50 * (10 ** 8),
        gas: 21000,
    })
    console.log(signedTx);
    const res = await ethBEP.sendSignedTransaction(signedTx.rawTransaction);
    console.log(res);
    console.log(`https://bscscan.com/tx/${res.transactionHash}`)
    return res.transactionHash;
};

/**
 * transfer a token from the account with the provided private key, to an address
 * @param amount amount of the token to transfer, in full units (not wei) for example 20 (USDT) - should be a number
 * @param contractAddress the contract address of the token (can get from coin market cap, make sure it's a bep20 contract address)
 * @param decimals the token decimals (most of the times 18)
 * @param toAddress the destination address
 * @param pk the private key of the sender account
 * @param shouldApprove should try to approve the contract (set to true if it's the first time you are using this method on the account with provided private key)
 * @returns {Promise<void>}
 */
export const transferBEPToken = async (amount, contractAddress, decimals, toAddress, pk, shouldApprove) => {
    const accountsBEP = new Accounts('https://bsc-dataseed1.binance.org:443');
    const ethBEP = new Eth("https://bsc-dataseed1.binance.org:443");
    console.log(`trying to transfer token ${contractAddress} with decimals ${decimals} amount ${amount} toAddress ${toAddress}`);
    const account = await accountsBEP.privateKeyToAccount(pk, true);
    console.log(account);
    const contractJson = fs.readFileSync(path.resolve(__dirname, `./bep20.abi.json`)).toString();
    const abi = JSON.parse(contractJson);

    const ethContract = new ethBEP.Contract(abi, contractAddress);
    const value = (amount * 0.98 * (10 ** decimals)).toString();

    const approve = async () => {
        console.log("sending approve tx");
        const approveData = ethContract.methods
            .approve(account.address.toLowerCase(), ethers.utils.parseUnits('1000.0', 18)).encodeABI();
        const signedTx = await account.signTransaction({
            gasPrice: 50 * (10 ** 8),
            gasLimit: 50000,
            to: contractAddress,
            value: 0,
            data: approveData
        });

        console.log(`signedTx ${signedTx}`);
        const res = await ethBEP.sendSignedTransaction(signedTx.rawTransaction);
        console.log("-------" + res);
        console.log(`approve tx https://bscscan.com/tx/${res.transactionHash}`)
    };

    if (shouldApprove) {
        await approve();
    }

    const data = ethContract.methods.transfer(toAddress, value).encodeABI();
    const transactionCount = await ethBEP.getTransactionCount(account.address);
    console.log("transactionCount", transactionCount);

    const signedTx = await account.signTransaction({
        gasLimit: ethers.utils.hexlify(200000),
        gasPrice: ethers.utils.parseUnits("10", "gwei"),
        to: contractAddress,
        value: 0,
        data: data,
        // nonce: transactionCount + 20
    });
    console.log(signedTx);
    const res = await ethBEP.sendSignedTransaction(signedTx.rawTransaction);
    console.log(res);
    console.log(`https://bscscan.com/tx/${res.transactionHash}`)
    await ethBEP.getTransactionReceipt(res.transactionHash);
    return res.transactionHash;
};

export const transferERCToken = async (amount, contractAddress, decimals, toAddress, pk, shouldApprove) => {
    const accountsERC = new Accounts('https://bsc-dataseed1.binance.org:443');
    const ethERC = new Eth("https://bsc-dataseed1.binance.org:443");

    console.log(`trying to transfer token ${contractAddress} with decimals ${decimals} amount ${amount} toAddress ${toAddress}`);
    const account = await accountsERC.privateKeyToAccount(pk, true);
    console.log(account);
    const contractJson = fs.readFileSync(path.resolve(__dirname, `./erc.abi.json`)).toString();
    const abi = JSON.parse(contractJson);

    const ethContract = new ethERC.Contract(abi, contractAddress);
    const value = (amount * 0.98 * (10 ** decimals)).toString();

    const approve = async () => {
        console.log("sending approve tx");
        const approveData = ethContract.methods
            .approve(account.address.toLowerCase(), ethers.utils.parseUnits('1000.0', 18)).encodeABI();
        const signedTx = await account.signTransaction({
            gasPrice: 50 * (10 ** 8),
            gasLimit: 50000,
            to: contractAddress,
            value: 0,
            data: approveData
        });

        console.log(`signedTx ${JSON.stringify(signedTx)}`);
        const res = await ethERC.sendSignedTransaction(signedTx.rawTransaction);
        console.log("-------" + res);
        console.log(`approve tx https://etherscan.io//tx/${res.transactionHash}`)
    };

    if (shouldApprove) {
        await approve();
    }

    const data = ethContract.methods.transfer(toAddress, value).encodeABI();
    const transactionCount = await ethERC.getTransactionCount(account.address);
    console.log("transactionCount", transactionCount);

    const signedTx = await account.signTransaction({
        gasLimit: ethers.utils.hexlify(200000),
        gasPrice: ethers.utils.parseUnits("10", "gwei"),
        to: contractAddress,
        value: 0,
        data: data,
        // nonce: transactionCount + 20
    });
    console.log(signedTx);
    const res = await ethERC.sendSignedTransaction(signedTx.rawTransaction);
    console.log(res);
    console.log(`https://etherscan.io//tx/${res.transactionHash}`)
    await ethERC.getTransactionReceipt(res.transactionHash);
    return res.transactionHash;
};
