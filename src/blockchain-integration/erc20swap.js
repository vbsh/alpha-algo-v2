
const ethers = require('ethers')

// const provider = new ethers.providers.InfuraProvider(
//     'mainnet',
//     '77e80bd0eb6b4f37b28ee0775ac0d926'
// )

const provider = new ethers.providers.WebSocketProvider('wss://mainnet.infura.io/ws/v3/77e80bd0eb6b4f37b28ee0775ac0d926');


const UNISWAP_ROUTER = '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D'.toLowerCase();


const addresses = {
    WETH: '0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2',
    recipient: `0xFFE71036334d1a7fCC02D2c2D0d60872aB3B2eB0`
}

const mnemonic = 'supreme slight awesome sea opera final double bargain settle buddy skate cruel';

const wallet = ethers.Wallet.fromMnemonic(mnemonic);
const account = wallet.connect(provider);

const router = new ethers.Contract(
    UNISWAP_ROUTER,
    [
        'function getAmountsOut(uint amountIn, address[] memory path) public view returns (uint[] memory amounts)',
        'function swapExactTokensForTokens(uint amountIn, uint amountOutMin, address[] calldata path, address to, uint deadline) external returns (uint[] memory amounts)'
    ],
    account
);

//We buy for 0.1 ETH of the new token
export const buyCoinNewVersion = async (token0, token1) => {
    //The quote currency needs to be WETH (we will pay with WETH)
    let tokenIn, tokenOut;
    if (token0 === addresses.WETH) {
        tokenIn = token0;
        tokenOut = token1;
    }

    if (token1 === addresses.WETH) {
        tokenIn = token1;
        tokenOut = token0;
    }

    //The quote currency is not WETH
    if (typeof tokenIn === 'undefined') {
        return;
    }

    //We buy for 0.1 ETH of the new token
    const amountIn = ethers.utils.parseUnits('0.01', 'ether');
    console.log("1")
    const amounts = await router.getAmountsOut(amountIn, [tokenIn, tokenOut],{gasLimit: ethers.utils.hexlify(200000),
        gasPrice: ethers.utils.parseUnits("158", "gwei")});
    console.log("2")
    //Our execution price will be a bit different, we need some flexbility
    const amountOutMin = amounts[1].sub(amounts[1].div(10));
    console.log("3")
    console.log(`
    Buying new token
    =================
    tokenIn: ${amountIn.toString()} ${tokenIn} (WETH)
    tokenOut: ${amounOutMin.toString()} ${tokenOut}
  `);
    const tx = await router.swapExactTokensForTokens(
        amountIn,
        amountOutMin,
        [tokenIn, tokenOut],
        addresses.recipient,
        Date.now() + 1000 * 60 * 20, //20 minutes
    );
    const receipt = await tx.wait();
    console.log('Transaction receipt');
    console.log(receipt);
}
