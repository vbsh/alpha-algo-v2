import awaitTransactionMined from "await-transaction-mined";
import Web3 from "web3";
import getTokenInfo from "./token-info";
import {tradingWalletAddress} from "../config";
const {Token, ChainId, Fetcher, WETH, Route, Trade, TokenAmount, TradeType, Percent} = require("@uniswap/sdk");
const {JsonRpcProvider} = require("@ethersproject/providers");
const provider = new JsonRpcProvider('https://cloudflare-eth.com/');
const ethers = require('ethers')
const UNISWAP_ROUTER = '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D'.toLowerCase();
const WETH_TOKEN_DECIMALS = 18;
const Eth = require("web3-eth")
const eth = new Eth("https://cloudflare-eth.com/");
const web3 = new Web3(new Web3.providers.HttpProvider('https://cloudflare-eth.com'));



async function generateWETHTrade(amountIn, tokenContractAddress, tokenDecimals, slippagePercents, tradingAddress, privateKey, outgoing) {
    const TRADING_TOKEN = new Token(
        ChainId.MAINNET,
        tokenContractAddress,
        tokenDecimals ? tokenDecimals : WETH_TOKEN_DECIMALS
    );

    const pair = await Fetcher.fetchPairData(TRADING_TOKEN, WETH[TRADING_TOKEN.chainId], provider);
    const route = new Route([pair], outgoing ? WETH[TRADING_TOKEN.chainId] : TRADING_TOKEN);
    const inAmountInWei = (amountIn * (10 ** (outgoing ? WETH_TOKEN_DECIMALS : tokenDecimals))).toString();

    const trade = new Trade(
        route,
        new TokenAmount(outgoing ? WETH[TRADING_TOKEN.chainId] : TRADING_TOKEN, inAmountInWei),
        TradeType.EXACT_INPUT
    );

    const slippageTolerance = new Percent(slippagePercents.toString(), '10000')

    // create transaction parameters
    // const amountOutMin = '0';  //Might be the issue


    const amountOutMin = trade.minimumAmountOut(slippageTolerance).raw
    console.log("amount out min", (amountOutMin / (10 ** (outgoing ? tokenDecimals : WETH_TOKEN_DECIMALS))).toString());
    const pathPair = [WETH[TRADING_TOKEN.chainId].address, TRADING_TOKEN.address];
    const path = outgoing ? pathPair : pathPair.reverse();
    const to = tradingAddress;
    const deadline = Math.floor(Date.now() / 1000) + 60 * 20

    // Create signer
    const wallet = new ethers.Wallet(
        Buffer.from(
            privateKey,
            "hex"
        )
    )
    const signer = wallet.connect(provider)

    return {TRADING_TOKEN, inAmountInWei, amountOutMin, path, to, deadline, signer};
}

async function approveUniswapTrading(TRADING_TOKEN, signer) {
    // Allow UNISWAP
    console.log("APPROVING !");

    const token_data = await getTokenInfo(TRADING_TOKEN,'ERC20');
    let abi = ["function approve(address _spender, uint256 _value) public returns (bool success)"]
    let contract = new ethers.Contract(TRADING_TOKEN.address, abi, signer)
    await contract.approve(UNISWAP_ROUTER, ethers.utils.parseUnits('0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff', token_data.decimals), {
        gasLimit: 200000,
        gasPrice: 5e9
    });
    console.log("contract approved");
}

/**
 * trade BNB for some token in exchange
 * @param wethAmountIn the amount of WETH you want to trade, in full units (not wei) for example: 0.03 (BNB) - should be a number
 * @param tradingAddress the trading address (should be the address of your account which you provide private key for)
 * @param privateKey the private key for the trading account
 * @param tokenContractAddress the contract address of the token
 * @param tokenDecimals the decimals of the token
 * @param slippagePercents percents for slippage (read more in docs/google)
 * @param shouldApprove should try to approve the contract (Set to true if it's the first time you are using this contract/method)
 * @returns {Promise<void>}
 */
export const tradeWETHToTokenInUniswap = async (wethAmountIn, tradingAddress, privateKey, tokenContractAddress, tokenDecimals, slippagePercents, shouldApprove) => {
    const {
        TRADING_TOKEN,
        inAmountInWei,
        amountOutMin,
        path,
        to,
        deadline,
        signer,
    } = await generateWETHTrade(wethAmountIn, tokenContractAddress, tokenDecimals, slippagePercents, tradingAddress, privateKey, true);

    const uniswap = new ethers.Contract(
        UNISWAP_ROUTER,
        ['function swapExactETHForTokens(uint256 amountOutMin, address[] path, address to, uint256 deadline) external payable returns (uint[] memory amounts)'],
        signer
    )

    if (shouldApprove) {
        await approveUniswapTrading(TRADING_TOKEN, signer);
    }

    // Execute transaction
    const tx = await uniswap.swapExactETHForTokens(
        ethers.utils.parseUnits((amountOutMin / (10 ** tokenDecimals)).toString(), tokenDecimals),
        path,
        to,
        deadline,
        {
            gasLimit: ethers.utils.hexlify(200000),
            gasPrice: ethers.utils.parseUnits("158", "gwei"),
            value: inAmountInWei
        }
    )

    console.log(`Tx-hash: ${tx.hash}`)
    console.log(`https://etherscan.io/tx/${tx.hash}`)
    await eth.getTransactionReceipt(tx.hash);
    return tx.hash;
};

/**
 * trade a token for BNB in exchange
 * @param tokenAmountIn the token amount that you want to trade, in full units (not wei), for example 20 (USDT) - should be a number
 * @param tradingAddress the address for the trading account (which you provide private key for)
 * @param privateKey the private key for the trading account
 * @param tokenContractAddress the contract address of the token
 * @param tokenDecimals the decimals of the token
 * @param slippagePercents percents for slippage
 * @param shouldApprove true if it's the first time you use this method for this account
 * @returns {Promise<void>}
 */
export const tradeTokenToWETHInUniswap = async (tokenAmountIn, tradingAddress, privateKey, tokenContractAddress, tokenDecimals, slippagePercents, shouldApprove) => {
    const {
        TRADING_TOKEN,
        amountOutMin,
        path,
        to,
        deadline,
        signer,
    } = await generateWETHTrade(tokenAmountIn, tokenContractAddress, tokenDecimals, slippagePercents, tradingAddress, privateKey, false);

    const uniswap = new ethers.Contract(
        UNISWAP_ROUTER,
        ['function swapExactTokensForETH(uint amountIn, uint amountOutMin, address[] calldata path, address to, uint deadline) external returns (uint[] memory amounts)'],
        signer
    )

    if (shouldApprove) {
        await approveUniswapTrading(TRADING_TOKEN, signer);
    }

    // Execute transaction
    const tx = await uniswap.swapExactTokensForETH(
        ethers.utils.parseUnits(tokenAmountIn.toString(), tokenDecimals),
        ethers.utils.parseUnits((amountOutMin / (10 ** WETH_TOKEN_DECIMALS)).toString(), WETH_TOKEN_DECIMALS),
        path,
        to,
        deadline,
        {gasLimit: ethers.utils.hexlify(200000), gasPrice: ethers.utils.parseUnits("158", "gwei")}
    )

    console.log(`Tx-hash: ${tx.hash}`)
    console.log(`https://bscscan.com/tx/${tx.hash}`)
    await eth.getTransactionReceipt(tx.hash);
    return tx.hash;
};

async function swapTokenForToken(tokenAmountIn, tokenInDecimals, route, TOKEN_IN, slippagePercents, tokenOutDecimals, TOKEN_OUT, tradingAddress, privateKey, shouldApprove) {
    const inAmountInWei = (tokenAmountIn * (10 ** tokenInDecimals)).toString();
    console.log(inAmountInWei)
    const trade = new Trade(
        route,
        new TokenAmount(TOKEN_IN, inAmountInWei),
        TradeType.EXACT_INPUT
    );

    const slippageTolerance = new Percent(slippagePercents.toString(), '10000')

    // create transaction parameters
    const amountOutMin = trade.minimumAmountOut(slippageTolerance).raw
    console.log("amount out min", (amountOutMin / (10 ** tokenOutDecimals)).toString());
    const path = [TOKEN_IN.address, TOKEN_OUT.address];
    const to = tradingAddress;
    const deadline = Math.floor(Date.now() / 1000) + 60 * 20

    // Create signer
    const wallet = new ethers.Wallet(
        Buffer.from(
            privateKey,
            "hex"
        )
    )
    const signer = wallet.connect(provider)

    const uniswap = new ethers.Contract(
        UNISWAP_ROUTER,
        ['function swapExactTokensForTokens(uint amountIn, uint amountOutMin, address[] calldata path, address to, uint deadline) external returns (uint[] memory amounts)'],
        signer
    )

    if (shouldApprove) {
        await approveUniswapTrading(TOKEN_IN, signer);
    }


    // // Execute transaction
    // const tx = await uniswap.swapExactTokensForTokens(
    //     ethers.utils.parseUnits(tokenAmountIn.toString(), tokenInDecimals),
    //     ethers.utils.parseUnits((amountOutMin / (10 ** tokenOutDecimals)).toString(), tokenOutDecimals),
    //     path,
    //     to,
    //     deadline,
    //     {gasLimit: ethers.utils.hexlify(200000), gasPrice: ethers.utils.parseUnits("158", "gwei")}
    // )
    //
    // console.log(`Tx-hash: ${tx.hash}`)
    // console.log(`https://etherscan.io/tx/${tx.hash}`)
    // console.log("Waiting for tx receipt");
    // await eth.getTransactionReceipt(tx.hash);
    // await awaitTransactionMined.awaitTx(web3, tx.hash);
    // return {txHash: tx.hash, minAmountOut: (amountOutMin / (10 ** tokenOutDecimals))};
}

/**
 * trade a token for another token in exchange
 * @param tokenAmountIn the token amount that you want to trade, in full units (not wei), for example 20 (USDT) - should be a number
 * @param tradingAddress the address for the trading account (which you provide private key for)
 * @param privateKey the private key for the trading account
 * @param tokenInContractAddress the contract address of the token you give away
 * @param tokenInDecimals the decimals of the token you give away
 * @param tokenOutContractAddress the contract address of the token you get
 * @param tokenOutDecimals the decimals of the token you give get
 * @param slippagePercents percents for slippage
 * @param shouldApprove true if it's the first time you use this method for this account
 * @returns {Promise<void>}
 */
export const tradeTokenForTokenInUniSwap = async (tokenAmountIn, tradingAddress, privateKey, tokenInContractAddress, tokenInDecimals,
                                                  tokenOutContractAddress, tokenOutDecimals, slippagePercents, shouldApprove) => {
    // console.log(slippagePercents)
    // const TOKEN_IN = new Token(
    //     ChainId.MAINNET,
    //     tokenInContractAddress,
    //     tokenInDecimals ? tokenInDecimals : WETH_TOKEN_DECIMALS
    // );
    //
    // const TOKEN_OUT = new Token(
    //     ChainId.MAINNET,
    //     tokenOutContractAddress,
    //     tokenOutDecimals ? tokenOutDecimals : WETH_TOKEN_DECIMALS
    // );
    // let route;


    try {
        console.log("--=-=-=-=-= NEW METHOD -=-=-=-=--");

        const tokenIn = await Fetcher.fetchTokenData(ChainId.MAINNET, tokenOutContractAddress, provider);
        const weth = WETH[tokenIn.chainId];
        const pair = await Fetcher.fetchPairData(tokenIn, weth, provider);

        const route = new Route([pair], weth);
        const trade = new Trade(route, new TokenAmount(weth, tokenAmountIn), TradeType.EXACT_INPUT);

        const slippageTolerance = new Percent('1', '100');
        const amountOM = trade.minimumAmountOut(slippageTolerance);
        const amountOutMin = toHex(amountOM);

        const path = [weth.address, tokenIn.address];
        const to = tradingWalletAddress;
        const deadline = Math.floor(Date.now()/1000) + 60*20;
        const value = toHex(trade.inputAmount);

        const signer = new ethers.Wallet(privateKey);
        const account = signer.connect(provider);
        const uniswap = new ethers.Contract(
            '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D',
            ['function swapExactETHForTokens(uint amountOutMin, address[] calldata path, address to, uint deadline) external payable returns (uint[] memory amounts)'],
            account
        );

        console.log('Swaping !')
        const tx = await uniswap.swapExactETHForTokens(
            amountOutMin,
            path,
            to,
            deadline,
            {value, gasPrice: ethers.utils.parseUnits("47", "gwei")} //Need To Verify Gas!
        );
        console.log(tx.wait());

        console.log("--=-=-=-=-=-=-=-=-=--");


        // console.log(`trying direct pair`);
        // const pair = await Fetcher.fetchPairData(TOKEN_IN, TOKEN_OUT, provider);
        // console.log(pair);
        // route = new Route([pair], TOKEN_IN);
        // console.log(`found direct route`);
        // return await swapTokenForToken(tokenAmountIn, tokenInDecimals, route, TOKEN_IN, slippagePercents, tokenOutDecimals, TOKEN_OUT, tradingAddress, privateKey, shouldApprove);
    } catch (e) {
        // console.log(`trying indirect pair`);
        // const WETHpair1 = await Fetcher.fetchPairData(TOKEN_IN, WETH[TOKEN_IN.chainId], provider);
        // console.log(WETHpair1)
        // console.log(`*****************-*-*-*-*-trying indirect pair2!!!!!!!!!!!!*************************************`);
        // const WETHpair2 = await Fetcher.fetchPairData(WETH[TOKEN_OUT.chainId], TOKEN_OUT, provider);
        // console.log(`*****************-*-*-*-*-trying indirect pair3!!!!!!!!!!!!*************************************`);
        // route = new Route([WETHpair1, WETHpair2], TOKEN_IN);
        // console.log(route);
        // console.log(`found indirect route`);
        // const {minAmountOut: wethAmount} = await swapTokenForToken(tokenAmountIn, tokenInDecimals, new Route([WETHpair1], TOKEN_IN), TOKEN_IN, slippagePercents, WETH_TOKEN_DECIMALS, WETH[TOKEN_IN.chainId], tradingAddress, privateKey, shouldApprove);
        // console.log(`now second tx`);
        // await swapTokenForToken(wethAmount, WETH_TOKEN_DECIMALS, new Route([WETHpair2], WETH[TOKEN_IN.chainId]), WETH[TOKEN_IN.chainId], slippagePercents, tokenOutDecimals, TOKEN_OUT, tradingAddress, privateKey, shouldApprove);
    }
};

function toHex(Amount) {
    return `0x${Amount.raw.toString(16)}`;
}

export const tradeUSDTForTokenInUniswap = async (tokenAmountIn, tradingAddress, privateKey,
                                                 tokenOutContractAddress, tokenOutDecimals, slippage) => {
    const usdtAddress = "0xdac17f958d2ee523a2206206994597c13d831ec7";
    // const wethAddress = "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2"

    return await tradeTokenForTokenInUniSwap(tokenAmountIn, tradingAddress, privateKey, usdtAddress, 6, tokenOutContractAddress, tokenOutDecimals, slippage, true)
}