import fs from "fs";
import path from "path";
import Eth from "web3-eth";

function getNetworkData(type) {
    let abiName;
    let eth;
    if (type === "ERC20") {
        abiName = `erc.abi.json`
        eth = new Eth("https://cloudflare-eth.com");
    } else {
        abiName = 'bep20.abi.json'
        eth = new Eth("https://bsc-dataseed1.binance.org:443");
    }
    return {abiName, eth};
}

export const getTokenInfo = async (tokenContractAddr, type) => {
    let {abiName, eth} = getNetworkData(type);
    console.log("got network data")
    const contractJson = fs.readFileSync(path.resolve(__dirname, `./${abiName}`)).toString();
    const abi = JSON.parse(contractJson);
    const tokenContract = new eth.Contract(abi, tokenContractAddr);
    console.log("created contract")
    const [decimals, name, symbol] = await Promise.all([
        tokenContract.methods.decimals().call(),
        tokenContract.methods.name().call(),
        tokenContract.methods.symbol().call(),
    ]);
    return {decimals, name, symbol};
}

export const getTokenBalance = async (tokenContractAddress, addressToCheck, type) => {
    let {abiName, eth} = getNetworkData(type);
    const contractJson = fs.readFileSync(path.resolve(__dirname, `./${abiName}`)).toString();
    const abi = JSON.parse(contractJson);
    const tokenContract = new eth.Contract(abi, tokenContractAddress);
    const tokenDecimals = await tokenContract.methods.decimals().call();
    return (await tokenContract.methods.balanceOf(addressToCheck).call()) / (10 ** tokenDecimals);
}