import Binance from "node-binance-api";
import {
    apiKey,
    apiSecret,
    binanceBEPWalletAddress, binanceERCWalletAddress,
    buyToCoin,
    moneyAmount,
    tradingWalletPrivateKey,
    tradingWalletAddress,
    slippage,
    stopLossPercentFromEntryPrice
} from "./config";
import {addCoinToHistory} from './file-utils';
import {
    //tradeBUSDForTokenInPancakeSwap,
    tradeUSDTForTokenInUniswap,
} from './blockchain-integration/uniswap'

import { 
    transferBEPToken, transferERCToken
} from './blockchain-integration/transfer'

import {
    getTokenBalance,
    getTokenInfo,
} from './blockchain-integration/token-info';
import {sendSms} from "../dist/utils/sms-utils";
import {buyCoinNewVersion} from "./blockchain-integration/erc20swap";

// const cypress = require('cypress')
const binance = new Binance().options({
    APIKEY: apiKey,
    APISECRET: apiSecret,
    test: false
});

// const phoneNumber = '0505201802';

export async function getPriceByCoinPer(coinPair) {
    let ticker = await binance.prices();
    if (!ticker[coinPair]) {
        throw new Error(`doesnt exist`)
    }

    return parseFloat(ticker[coinPair]);
}

async function buyCoin(coinPair, buyInUSDTAmount) {
    let price = await getPriceByCoinPer(coinPair).catch((e) => {
        console.log(e);
    });

    let quantity = parseFloat(buyInUSDTAmount) / parseFloat(price.toString());
    let formattedQuantity = parseFloat(quantity.toPrecision(5))
    const roundedQuantity = Math.floor(formattedQuantity)
    console.log("start buying :\n" +
        "{\n" +
        "   coin pair: " + coinPair + " \n" +
        "   price: " + price + "\n" +
        "   roundedQuantity: " + roundedQuantity + "\n" +
        "   formattedQuantity: " + formattedQuantity + "\n" +
        "   quantity: " + quantity + "\n" +
        "   buyInUsdtAmount: " + buyInUSDTAmount + "\n" +
        "}"
    )
    await binance.marketBuy(coinPair, roundedQuantity)
    await setStopLoss(roundedQuantity, coinPair, price * stopLossPercentFromEntryPrice)

    let balance = await binance.balance();
    let coinAvailableBalance = balance[coinPair.replace('USDT', '')].available
    let quantityToSell = Math.floor(parseFloat(coinAvailableBalance))
    return {
        coinAvailableBalance: quantityToSell,
        price: parseFloat(price.toString()),
        stoploss: stopLossPercentFromEntryPrice,
    };
}

async function generateVolume(coinPair, quantity) {
    let pairsInformation = await binance.exchangeInfo()
    const desiredPairInfo = pairsInformation.symbols.filter((pair) => {
        return pair.symbol === coinPair
    })

    const info = (desiredPairInfo[0].filters[2]);
    const stepSize = info['stepSize']
    return quantity * stepSize + parseFloat(info["minQty"]);
}

async function getMinVolume(coinPair) {
    let pairsInformation = await binance.exchangeInfo()
    const desiredPairInfo = pairsInformation.symbols.filter((pair) => {
        return pair.symbol === coinPair
    })

    const info = (desiredPairInfo[0].filters[3]);
    const min = info["minNotional"]
    return parseFloat(min);
}

async function sellCoin(coinPair, roundedQuantity) {
    await binance.marketSell(coinPair, roundedQuantity)
    console.log("coin sold !!!!!!")
}

async function buyAndSell(coinPair, buyInUsdtAmount) {
    await buyCoin(coinPair, buyInUsdtAmount)
    await sellCoin(coinPair, buyInUsdtAmount)
}

async function setStopLoss(quantiy, coinPair, stopPrice) {
    let price = await getPriceByCoinPer(coinPair);
    binance.sell(coinPair, quantiy, price, {stopPrice: stopPrice, type: "STOP_LOSS"})
}

export async function trySellCoin(coinToBuy, tokenAddress) {
    let coinPair = coinToBuy + buyToCoin;
    let coinAvailableBalance;
    let shouldSellCoin = true;
    let tokenDecimals;
    let tokenBalance;

    //SMS WITH TXT
    let now = new Date();

    console.log(`token balance for contract address ${tokenAddress}`);

    console.log(`coinPair: ${coinPair}`)
    try {
        await tradeUSDTForTokenInUniswap(moneyAmount, tradingWalletAddress, tradingWalletPrivateKey, tokenAddress, tokenDecimals, slippage) //Switched between Try And Catch. - Need To Upgrade Scraoer and system.

        // tokenDecimals = (await getTokenInfo(tokenAddress, 'BEP20')).decimals;
        // await tradeBUSDForTokenInPancakeSwap(moneyAmount, tradingWalletAddress, tradingWalletPrivateKey, tokenAddress, tokenDecimals, slippage, true)
        // tokenBalance = await getTokenBalance(tokenAddress, tradingWalletAddress, 'BEP20');
        // console.log(`token balance for contract address ${tokenAddress} is ${tokenBalance}`);
        // let transferFinished = false;
        // while (!transferFinished) {
        //     try {
        //         await transferBEPToken(tokenBalance, tokenAddress, tokenDecimals, binanceBEPWalletAddress, tradingWalletPrivateKey, true)
        //         transferFinished = true;
        //         now = new Date();
        //         sendSms(`BoTrader just bought |${coinPair}| for ${moneyAmount}$ |[${now}]| `, '0505201802');
        //     } catch (e) {
        //         console.log(e);
        //     }
        // }
    } catch (e) {
    //     console.log("try uniswap");
    //     now = new Date();
    //     //sendSms(`[Pancakeswap Faild] - Trying UniSwap for |${coinPair}| |[${now}]| `, '0505201802'); //;0546103608;0548060891;0536205747;0548045513
    //
    //     // tokenDecimals = (await getTokenInfo(tokenAddress, 'ERC20')).decimals;
    //     // console.log(`token decimals for uniswap are: ${tokenDecimals}`);
    //     await tradeUSDTForTokenInUniswap(moneyAmount, tradingWalletAddress, tradingWalletPrivateKey, tokenAddress, tokenDecimals, slippage)
    //
    //     // await buyCoinNewVersion(`0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2` , tokenAddress);
    //
    //     tokenBalance = await getTokenBalance(tokenAddress, tradingWalletAddress, 'ERC20');
    //     console.log(`token balance for contract address ${tokenAddress} is ${tokenBalance}`);
    //     let transferFinished = false;
    //     while (!transferFinished) {
    //         try {
    //             await transferERCToken(tokenBalance, tokenAddress, tokenDecimals, binanceERCWalletAddress, tradingWalletPrivateKey, true)
    //             transferFinished = true;
    //             now = new Date();
    //             sendSms(`BoTrader just bought |${coinPair}| for ${moneyAmount}$ |[${now}]| `, '0505201802');
    //         } catch (e) {
    //             console.log(e);
    //         }
    //     }
    // }
    // while (shouldSellCoin) {
    //     try {
    //         coinAvailableBalance = await getCoinBalance(coinToBuy);
    //         console.log("try to sell coin \n parms: { \n" +
    //             "coinPair: " + coinPair + " \n" +
    //             "roundedQuantity: " + coinAvailableBalance + " \n;")
    //         console.log(`min value to sell ${await getMinVolume(coinPair)}`);
    //         await sellCoin(coinPair, coinAvailableBalance)
    //         addCoinToHistory(coinToBuy);
    //         now = new Date();
    //         sendSms(`Sold:${coinAvailableBalance} \n ${coinToBuy} Coin Added To history. \n The Trade Is Over. |[${now}]| `, '0505201802');
    //         shouldSellCoin = false;
    //     } catch (e) {
    //         console.log(e)
    //         await delay(1000);
    //         console.log("faild to sell coin");
    //     }
    }
}

async function getCoinBalance(coinName) {
    let balance = await binance.balance();
    return Math.floor(parseFloat(balance[coinName].available))
}

async function delay(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

