const cypress = require('cypress')

cypress
    .run({
        // the path is relative to the current working directory
        spec: './cypress/integration/tradecoin.spec.js',
    })
    .then((results) => {
        console.log("------------------------------")
        console.log(results)
    })
    .catch((err) => {
        console.error(err)
    })
